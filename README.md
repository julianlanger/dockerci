# Docker container for gitlab testing
To rebuild a docker container run 
`docker build -t registry.gitlab.com/julianlanger/dockerci:<phpVersion> --file <Dockerfile> .`
> Example: `docker build -t registry.gitlab.com/julianlanger/dockerci:7.3 --file Dockerfile7.3 .`

With the according docker file/version you want to rebuild. Then push the image to the registry with the following commands:
```bash
# This needs to only be run once after start up. Enter the gitlab credentails:
docker login registry.gitlab.com

docker push registry.gitlab.com/julianlanger/dockerci:<phpVersion>
```
