#!/bin/bash

if [ $# -eq 0 ];
then
    VERSIONS=$(ls Dockerfile*)
else
    VERSION_FROM_ARG=$(echo "$1"|tr '.' '_')
    VERSIONS="$VERSION_FROM_ARG"
fi

docker login registry.gitlab.com
for version in "$VERSIONS"
do
    DOTTED_VERSION=$(echo "$version"|tr '_' '.')
    docker build -t registry.gitlab.com/julianlanger/dockerci:$DOTTED_VERSION --file Dockerfile$version .
    docker push registry.gitlab.com/julianlanger/dockerci:$DOTTED_VERSION
done
